moon-atom
=========

a fediverse bot for posting updates from Atom feeds, written in Lua

basic usage
-----------

* Install dependencies:
`# luarocks install moon_atom-1.0-0.rockspec`
* Run `$ mkdir priv`.
* Run `$ lua setup.lua`. You'll be asked some basic questions, and it will
create a file in `priv`.
* Run `$ lua bot.lua priv/user_[...]` to run the script. It is best run from cron.
