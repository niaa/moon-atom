local curl = require("cURL")
local json = require("cjson.safe")

mastodon = {}

function make_post_fields(fields)
	local args = {}
	for k, v in pairs(fields) do
		table.insert(args, string.format("%s=%s", k, v))
	end
	return table.concat(args, "&")
end

function post_get_json(auth, url, fields)
	local header = nil
	if auth.access_token then
		header = { string.format("Authorization: Bearer %s", auth.access_token) }
	end
	local request = curl.easy {
		url = string.format("https://%s/%s", auth.instance, url),
		postfields = make_post_fields(fields),
		post = true, httpheader = header
	}
	local queue = {}
	request:setopt_writefunction(function(buffer)
		table.insert(queue, buffer)
	end)
	request:perform()
	request:close()
	print(table.concat(queue, ""))
	return json.decode(table.concat(queue, ""))
end

function register_app(instance, request)
	if request.scopes then
		request.scopes = string.gsub(request.scopes, " ", "%20")
	end
	return post_get_json({ instance = instance }, "/api/v1/apps", request)
end

function auth_url(instance, redirect_uri, scopes, client_id)
	return string.format("https://%s/oauth/authorize?%s", instance,
	    make_post_fields({
		scope = string.gsub(scopes, " ", "%20"),
		response_type = "code",
		redirect_uri = redirect_uri,
		client_id = client_id
	    }))
end

function oauth_token(instance, request)
	return post_get_json({ instance = instance }, "/oauth/token", request)
end

function post_status(auth, status)
	return post_get_json(auth, "/api/v1/statuses", status)
end

return {
	register_app = register_app,
	auth_url = auth_url,
	oauth_token = oauth_token,
	post_status = post_status,
}
