package = "moon_atom"
version = "1.0-0"
source = { url = "." }
dependencies = {
	"cjson2",
	"feedparser",
	"lua-curl",
	"stdlib",
	"std.normalize"
}
build = {
	type = "builtin",
	modules = {
		"bot.lua",
		"mastodon.lua",
		"setup.lua"
	}
}
