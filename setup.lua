local mastodon = require("mastodon")

local CLIENT_NAME = "moon_atom"
local WEBSITE = "https://gitlab.com/niaa/moon-atom"

local instance, instance_dotless, name, token, feed_url
local app_filename, app_file

io.write("Instance (e.g. beeping.town): ")
instance = io.read("*line")

instance_dotless = string.gsub(instance, "%.", "_")

app_filename = string.format("priv/app_%s_%s.lua",
    instance_dotless, CLIENT_NAME)

app_file = io.open(app_filename, "r")
if not app_file then
	local response = mastodon.register_app(instance, {
		client_name = CLIENT_NAME,
		website = WEBSITE,
		redirect_uris = "urn:ietf:wg:oauth:2.0:oob",
		scopes = "write"
	})
	app_file = assert(io.open(app_filename, "w"))
	app_file:write(string.format("INSTANCE = \"%s\"\n",
	    instance))
	app_file:write(string.format("CLIENT_ID = \"%s\"\n",
	    response.client_id))
	app_file:write(string.format("CLIENT_SECRET = \"%s\"\n",
	    response.client_secret))
end
app_file:close()

require(string.sub(app_filename, 0, #app_filename - 4))

print(string.format("Visit this URL: %s",
    mastodon.auth_url(instance, "urn:ietf:wg:oauth:2.0:oob",
	"write", CLIENT_ID)))

io.write("Token code: ")
token = io.read("*line")

local response = mastodon.oauth_token(instance, {
	redirect_uri = "urn:ietf:wg:oauth:2.0:oob",
	client_id = CLIENT_ID,
	client_secret = CLIENT_SECRET,
	grant_type = "authorization_code",
	code = token
})

io.write("Bot name (used for filename): ")
name = io.read("*line")

io.write("Feed URL: ")
feed_url = io.read("*line")

local user_file = assert(io.open(string.format("priv/user_%s_%s.lua",
    instance_dotless, name), "w"))
user_file:write(string.format("require(\"priv/app_%s_%s\")\n",
    instance_dotless, CLIENT_NAME))
user_file:write(string.format("ACCESS_TOKEN = \"%s\"\n",
    response.access_token))
user_file:write(string.format("FEED_URL = \"%s\"\n",
    feed_url))
user_file:close()
