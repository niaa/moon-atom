local bot_path = string.sub(arg[1], 0, #arg[1] - 4)

require(bot_path)

local mastodon = require("mastodon")
local curl = require("cURL")
local feedparser = require("feedparser")
local last_newest = os.time()

local auth = {
	instance = INSTANCE,
	access_token = ACCESS_TOKEN
}

local time_file = io.open(bot_path .. ".time", "r")
if time_file then
	last_newest = time_file:read("n")
	time_file:close()
end

local temp = {}
connection = curl.easy { url = FEED_URL }
connection:setopt_writefunction(function(buffer)
	table.insert(temp, buffer)
end)
connection:perform()
connection:close()

local parsed = feedparser.parse(table.concat(temp, ""))
local to_post = {}
local newest_time = last_newest

for i = 1, #parsed.entries do
	local updated_time = parsed.entries[i].updated_parsed
	local title = string.gsub(parsed.entries[i].title, "^[%s%c]+", "")
	title = string.gsub(title, "[%s%c]+$", "")
	local link = parsed.entries[i].link
	if updated_time > last_newest then
		table.insert(to_post, {
			status = string.format("%s\n\n%s", title, link)
		})
	end
	if updated_time > newest_time then
		newest_time = updated_time
	end
end

last_newest = newest_time

for i = 1, #to_post do
	to_post[i].visibility = #to_post > 1 and "unlisted" or "public"
	local response = mastodon.post_status(auth, to_post[i])
	for k, v in pairs(response) do
		print(string.format("%s: %s", k, v))
	end
end

time_file = io.open(bot_path .. ".time", "w")
if time_file then
	time_file:write(string.format("%d\n", last_newest))
	time_file:close()
end
